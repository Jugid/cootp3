from tp2 import *

#on veut pouvoir créer des boites
def test_box_create():
	b=Box()
	b.add("truc1")
	b.add("truc2")
	"truc2" in b
	b.delete("truc2")
	b.isOpen()
	b.close()
	b.isOpen()
	b.open()
	b.isOpen()
	b.action_look()
	a=Box()
	a.action_look()
	t=Thing(3)
	t.has_name(None)
	t2=Thing(3,"ouiPapa")
	b.set_capacity(8)
	b.has_room_for(t)
	b.open()
	b.empty()
	b.action_add(t2)
	t2.has_name("ouiPapa")
	b.find("ouiPapa")
	b.find("ouiMaman")
	
