class Box:
	def __init__(self, is_open=True, capacity=None):
		self._contents = contents
		self.state=is_open
		self.capacity=capacity

	def add(self, truc):
		self._contents.append(truc)
		if self.capacity!=None:
			self.capacity-=truc.get_volume()

	def __contains__(self,truc):
		return truc in self._contents

	def delete(self,truc):
		self._contents.remove(truc)

	def isOpen(self):
		return self.state

	def close(self):
		self.state=False

	def open(self):
		self.state=True

	def action_look(self):
		if self.isOpen():
			s="la boite contient : "+", ".join(self._contents)
			print(s)
		else:
			print("la boite est fermee")

	def set_capacity(self,capacity):
		self.capacity=capacity

	def get_capacity(self):
		return self.capacity

	def has_room_for(self,t):
		if self.capacity == None:
			return True
		return self.capacity >= t.get_volume()

	def action_add(self,t):
		if self.has_room_for(t) and self.isOpen():
			self.add(t)
			return True
		else:
			return False

	def find(self,name):
		if self.isOpen():
			for truc in self._contents:
				if truc.has_name(name):
					return True
		return None

	def empty(self):
		self._contents = []

	@staticmethod
	def from_yaml(data):
		state = data.get("state", True)
		capacity = data.get("capacity", None)
		return Box(is_open=state, capacity=capacity)

class Thing:
	def __init__(self,volume,name=None):
		self.volume=volume
		self.name=name

	def get_volume(self):
		return self.volume

	def set_name(self,name):
		self.name=name

	def __repr__(self):
		return self.name

	def has_name(self,name):
		return self.name == name

	@staticmethod
	def from_yaml(data):
		volume = data.get("volume", None)
		name = data.get("name", None)
		return Thing(volume=volume, name=name)

class Person:
	def __init__(self, firstname=None, lastname=None):
		self.firstname = firstname
		self.lastname = lastname

	@staticmethod
	def from_data(data):
		f = data.get("firstname", None)
		l = data.get("lastname", None)
		return Person(firstname=f, lastname=l)

